//==============================================================================
//	
//	Copyright (c) 2002-
//	Authors:
//	* Dave Parker <david.parker@comlab.ox.ac.uk> (University of Oxford)
//	
//------------------------------------------------------------------------------
//	
//	This file is part of PRISM.
//	
//	PRISM is free software; you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation; either version 2 of the License, or
//	(at your option) any later version.
//	
//	PRISM is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with PRISM; if not, write to the Free Software Foundation,
//	Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//	
//==============================================================================

package pta;

import java.util.*;

import explicit.*;
import prism.*;

/**
 * Class to store a forwards reachability graph for a PTA.
 */
public class ReachabilityGraph
{
	// PTA
	PTA pta;
	// List of symbolic states (Z)
	List<LocZone> states;
	// List of symbolic transitions (grouped by source location)
	// (thus source is not stored explicitly in the symbolic transition)
	ArrayList<ArrayList<SymbolicTransition>> trans;

	/**
	 * Construct empty reachability graph.
	 * @param pta PTA associated with this graph.
	 */
	public ReachabilityGraph(PTA pta)
	{
		this.pta = pta;
		states = null;
		trans = new ArrayList<ArrayList<SymbolicTransition>>();
	}

	public void addState()
	{
		trans.add(new ArrayList<SymbolicTransition>());
	}

	public void copyState(int i)
	{
		ArrayList<SymbolicTransition> listOld, listNew;
		listOld = trans.get(i);
		listNew = new ArrayList<SymbolicTransition>(listOld.size());
		for (SymbolicTransition g : listOld)
			listNew.add(new SymbolicTransition(g));
		trans.add(listNew);
	}


	//duplicate the incoming transitions(set the self-loops destinations to the new state), add a new state to the graph
	public void myCopyState(int i)
	{
		//loczone
		// Number of location
		int loci = pta.locationNames.size();

		//Add a location with a copy name
		pta.locationNames.add(pta.locationNames.size(), "CP_"+loci+ pta.getLocationNameString(states.get(i).loc));

		states.add(new LocZone(loci, states.get(i).zone));

		//outgoing transition

		ArrayList<SymbolicTransition> listNew;
		listNew = new ArrayList<SymbolicTransition>(trans.get(i).size());
		for (SymbolicTransition g : trans.get(i)){
			listNew.add(new SymbolicTransition(g));
		}
		trans.add(listNew);

		//incoming transition
		for (int j = 0; j < this.trans.size(); j++){
			int sizeInit = trans.get(j).size();
			for (int k2 = 0; k2 < sizeInit; k2++){
				SymbolicTransition st = trans.get(j).get(k2);
				if (st.hasSuccessor(i)){
					SymbolicTransition st2 = new SymbolicTransition(st);
					int k = 0;
					while(st2.dests[k] != i){k++;}
					st2.dests[k] = trans.size() - 1;
					trans.get(j).add(st2);
				}
			}
		}

	}


	/**
	 * Add a new symbolic transition to the graph.
	 * @param src Source state index
	 * @param tr Corresponding transition in the PTA
	 * @param dests List of destinations
	 * @param valid Validity condition (usually left null and computed later)
	 */
	public void addTransition(int src, Transition tr, int dests[], Zone valid)
	{
		trans.get(src).add(new SymbolicTransition(tr, dests, valid));
	}

	/**
	 * Returns true if state s2 is a successor of state s1
	 * (i.e. if there is a symbolic transition with s1 as source and s2 in destinations).
	 */
	public boolean isSuccessor(int s1, int s2)
	{
		for (SymbolicTransition st : trans.get(s1)) {
			if (st.hasSuccessor(s2))
				return true;
		}
		return false;
	}

	/**
	 * Compute the validity conditions for all symbolic transitions in the graph.
	 */
	public void computeAllValidities()
	{
		int i, n;
		ArrayList<SymbolicTransition> list;
		n = trans.size();
		for (i = 0; i < n; i++) {
			list = trans.get(i);
			for (SymbolicTransition st : list) {
				st.valid = computeValidity(i, st.tr, st.dests);
			}
		}
	}

	/**
	 * Print the list of symbolic states to a log.
	 */
	public void printStates(PrismLog log)
	{
		int i = 0;
		for (LocZone lz : states) {
			if (i > 0)
				log.print(" ");
			log.print("#" + (i++) + ":" + lz);
		}
	}
	@Override
	public String toString()
	{
		int i, n, m;
		boolean first;
		String s;
		n = trans.size();
		first = true;
		s = "[ ";
		for (i = 0; i < n; i++) {
			if (first)
				first = false;
			else
				s += ", ";
			s += i + ":" + trans.get(i);
			m = trans.get(i).size();
			for (int j = 0; j < m; j++){
				//s+=trans.get(i).get(j).tr.toString();s+="  \n";

				s+=DBM.createFromTransition(trans.get(i).get(j).tr).toString();

				s+="  ";
			}
			s += "zone :";
			s += states.get(i).zone.toString() + "\n";
		}
		s += " ]";
		return s;
	}

	/**
	 * Compute the validity condition for a symbolic transition
	 * @param src The index of the source state of the symbolic transition 
	 * @param tr The PTA transition corresponding to the symbolic transition
	 * @param dests The list of destination state indices of the symbolic transition
	 */
	public Zone computeValidity(int src, Transition tr, int[] dests)
	{
		Zone z = new DBMList(DBM.createTrue(pta));
		int count = 0;
		for (Edge edge : tr.getEdges()) {
			LocZone lz2 = states.get(dests[count]).deepCopy();
			lz2.dPre(edge);
			z.intersect(lz2.zone);
			count++;
		}
		z.down();
		z.intersect(states.get(src).zone);

		return z;
	}
	
	/**
	 * Build an MDP from this forwards reachability graph.
	 * The set of initial states should also be specified.
	 */
	public MDP buildMDP(List<Integer> initialStates) throws PrismException
	{
		Distribution distr;
		int numStates, src, count, dest;
		MDPSimple mdp;

		// Building MDP...
		mdp = new MDPSimple();

		// Add all states
		mdp.addStates(states.size());

		// For each symbolic state...
		numStates = states.size(); 
		for (src = 0; src < numStates; src++) {
			// And for each outgoing transition in PTA...
			for (SymbolicTransition st : trans.get(src)) {
				distr = new Distribution();
				count = -1;
				for (Edge edge : st.tr.getEdges()) {
					count++;
					dest = st.dests[count];
					distr.add(dest, edge.getProbability());
				}
				if (!distr.isEmpty())
					mdp.addChoice(src, distr);
			}
		}

		// Set initial states
		for (int i : initialStates) {
			mdp.addInitialState(i);
		}

		return mdp;
	}

	//computes all the transitions going into a location (returns the transition without its source)
	public LinkedList<TransitionIndex> predecessors(int i){
		LinkedList<TransitionIndex> res = new LinkedList<TransitionIndex>();
		for (int k= 0;k < trans.size(); k++){
			ArrayList<SymbolicTransition> l = trans.get(k);
			for(int k2 = 0; k2 <l.size() ; k2++){
				SymbolicTransition tr = l.get(k2);
				for(int j : tr.dests){
					if (j == i){
						res.add(new TransitionIndex(k,k2));	//does not give a copy of the transition
					}
				}
			}
		}
		return res;
	}

	/**
	 *
	 * @param down_guard : TE-1 (TE(zone) \inter g_delta)
	 * @return list : \overline{TE-1 (TE(zone) \inter g_delta)} \inter zone
	 */
	private ArrayList<DBM> interSplit(Zone z,DBM down_guard){
		DBMList z20 = new DBMList(down_guard.deepCopy());
		DBMList comp_down_guard = z20.createComplement(); // \overline{ down_guard )}
		comp_down_guard.intersect(z); // \overline{TE-1 (TE(zone) \inter g_delta)} \inter zone
		ArrayList<DBM> list = comp_down_guard.getList();

		//Opening zones and removing empty ones
		for(int i2 = 0; i2 < list.size(); i2++){
			if(list.get(i2).openIt(z))
			{list.remove(i2);i2=i2-1;}
		}
		return list;
	}


	/***
	 *
	 * @param i index of the current locZone
	 * @param z zone to split (\delta)
	 * @param down_guard TE-1 of the guard inducing the split
	 * @param cand the set of candidate where to add new transitions
	 */
	public void splitZone( int i,Zone z,DBM down_guard,TreeSet<TransitionIndex> cand){
		ArrayList<DBM> list = interSplit(z, down_guard); //\overline{TE-1 (TE(zone) \inter g_delta)} \inter zone
		if (list.size() > 0) {
			//System.out.printf("\nZONE SPLIT: une list pour etat %d et trans %d: %s \n",i,k, list.toString());
			for (DBM dbm : list) {
				//we create list.size new states
				myCopyState(i);   // creation of the state, add all new transition to cand
				states.get(states.size() - 1).zone = dbm;
				update_pta_state(states.size() - 1, cand);
			}
			//The current zone z is still to be treated...
			Zone newz = z.deepCopy();
			newz.intersect(down_guard);
			states.get(i).zone = newz;//the current zone is restricted, the remainder of the zone (called list above) has already been treated.

			update_pta_state(i, cand);  //TODO do not add to cand outgoing transition from current state ?
		}
	}

	/***
	 * @param state Index of the new loczone in the states list
	 * @param cand the set of candidate where to add new transitions
	 */
	public void update_pta_state(int state,TreeSet<TransitionIndex> cand){
		Zone zUp = states.get(state).zone.deepCopy();
		zUp.up();
		for(int j=0; j<trans.get(state).size(); j++) { //outgoing transition
			SymbolicTransition tran = trans.get(state).get(j);
			tran.modifyConstraint(zUp.toConstraint());
			TransitionIndex ci = new TransitionIndex(state, j);
			DBM guard = DBM.createFromTransition(tran.tr);
			if( !guard.isEmpty()) {
				cand.add(ci);
			} else {
				cand.remove(ci);
			}
		}

		LinkedList<TransitionIndex> pr_current = predecessors(state); //list of predecessor transitions
		for(TransitionIndex pr : pr_current){ //incoming transition
			Zone zbackR = states.get(state).zone.deepCopy();
			SymbolicTransition ptr = pr.getTrans(this);
			int ind=-1;
			for (Edge edge : ptr.tr.getEdges()){ //Currently only work for TA
				// Each edge should have same reset in pta too.
				ind++;
				if (ptr.dests[ind]== state){//TODO assuming that dests preserves the order of Edges.
					for (Map.Entry<Integer,Integer> e : edge.getResets()){
						zbackR.backReset(e.getKey(), e.getValue());
					}
				}
			}
			ptr.modifyConstraint(zbackR.toConstraint());
			DBM guard = DBM.createFromTransition(ptr.tr);
			if( !guard.isEmpty()) {
				cand.add(pr);
			} else {
				cand.remove(pr);
			}
		}

	}

	class TransitionIndex implements Comparable<TransitionIndex> {
		public int state;
		public int actionNb;
		TransitionIndex(int s,int a){
			this.state =s;
			this.actionNb = a;
		}
		SymbolicTransition getTrans(ReachabilityGraph p){
			return p.trans.get(state).get(actionNb);
		}

		@Override
		public int compareTo(TransitionIndex ti) {
			if (this.state == ti.state && this.actionNb == ti.actionNb )
				return 0;
			else if (this.state > ti.state ||
					(this.actionNb > ti.actionNb
							&& this.state == ti.state
					))
				return 1;
			else
				return -1;
		}

		public String toString(){
			return "TransitionIndex("+state+","+actionNb+")";
		}
	}

	/***
	 *	Assume the graph is the forward reachability graph.
	 *	Split it.
	 */
	public void split(){
		// l1
		TreeSet<TransitionIndex> cand = new TreeSet<TransitionIndex>();
		for(int i = 0; i < this.trans.size(); i++){ //Initialize candidate with all transitions index.
			for(int j = 0; j< this.trans.get(i).size();j++){
				cand.add(new TransitionIndex(i,j));
			}
		}
		//l2
		while(!cand.isEmpty()){

			//l3
			TransitionIndex deltaId = cand.first();
			//System.out.printf("\tstate: %s trans: %s\n", states.get(deltaId.state).toString(), trans.get(deltaId.state).get(deltaId.actionNb).reducetoString());

			cand.remove(deltaId);
			SymbolicTransition delta = deltaId.getTrans(this);

			//l4
			Zone z = this.states.get(deltaId.state).zone;
			Zone z2 = z.deepCopy();
			z2.up();
			delta.modifyConstraint(z2.toConstraint());//intersect the guard with z2=TE(zone)
			DBM guard = DBM.createFromTransition(delta.tr); // guard = TE(zone) \inter g_delta
			if (!guard.isEmpty()){ //should always be true
				DBM down_guard = guard.deepCopy();			
				down_guard.down(); // TE-1 (TE(zone) \inter g_delta)
				
				//l5
				splitZone(deltaId.state,z,down_guard,cand); //Call to SplitZone(\delta)

				//SECOND STEP: guard split
				//TODO check whether canonisation is necessary
				//l6
				DBMandBound guardandbound = new DBMandBound(guard);
				LinkedList<DBM> l = guardandbound.mySplit(); //Call to SplitGuard(\delta)
				//l7
				if (l.size()>1){
					//l8
					for (int j= 0; j < l.size(); j++){
						int newState = deltaId.state;
						if (j < l.size() - 1) {
							//if not the last time create a new state
							this.myCopyState(deltaId.state);
							newState = states.size() - 1;
						}
						DBM guard_current = l.get(j);
						guard_current.down();
						Zone z5 = z.deepCopy();
						z5.intersect(guard_current);
						LocZone newStateInst =states.get(newState);
						newStateInst.zone = z5;
						update_pta_state(newState,cand);
					}
				}
			}
		}
	}

	//assuming that the graph is splitted reducing the guard
	public String reducetoString(){
		String s="ptavalue={ \"statelist\":[\n";
		int n=this.states.size();
		//boolean firststate=true;
		for(int i = 0; i < n; i++){
			StringBuilder sb = new StringBuilder();
			sb.append("{\n\t\"id\": "+ i +",\n\t\"transition\":["); //s+="[";

			LocZone lz = this.states.get(i);
			Zone z=lz.zone;
			int initialNumberTransition = this.trans.get(i).size();
			boolean firsttrans=true;

			for (int k = 0; k < initialNumberTransition; k++){
				SymbolicTransition tran=this.trans.get(i).get(k);
				DBM guard = DBM.createFromTransition(tran.tr);
				if (!guard.isEmpty()){
					if (!firsttrans){sb.append(", ");};
					firsttrans=false;
										
					sb.append("\n\t\t{");
					sb.append("\"zone\": [" + tran.reducetoString() +"]");
					sb.append(", \"zone_string\": \"[" + guard +"]\"");
					int target = tran.tr.getEdges().get(0).getDestination();
					//Zone targetzone = states.get(target).zone.deepCopy();
					Zone guardzone = guard.deepCopy();
					//for (Map.Entry<Integer,Integer>e : tran.tr.getEdges().get(0).getResets()) {
					//	guardzone.reset(e.getKey(), e.getValue());
					//}
					guardzone.intersect(z);
					sb.append(", \"is_zone_contained\": "+ !guardzone.isEmpty() +"");
					sb.append(", \"action\":\"" + tran.tr.getAction().toString() + "\"");
					int ind=-1;
					sb.append(", \"miniedge\": [");
					boolean firstedge=true;
					for(Edge edge : tran.tr.getEdges()) {
						ind++;
						int dest = tran.dests[ind];
						if (!firstedge) {
							sb.append(", ");
						}
						firstedge = false;
						sb.append("{ \"target\" :" + dest +", " + edge.toStringSage() + "}");
					}
					sb.append("]}");
				}
			}
			

			if (firsttrans){
				System.out.println("the state"+this.states.get(i).toString()+"has no outgoing transition sink, states added");
				if (i>0) s+=",\n";
				s+= sb.toString()+"],\n";
				s+= "\t\"name\": \"" + this.states.get(i).toString() +  "\", ";
				s+= "\n\t\"redcoord\": " + z.reducedCoordinate().toString() + "\n}";
			}else{
				if (i>0) s+=",\n";
				s+= sb.toString()+"],\n";
				s+= "\t\"name\": \"" + this.states.get(i).toString() +  "\", ";
				s+= "\n\t\"redcoord\": " + z.reducedCoordinate().toString() + "\n}";
			}
		}
		s+="]";
		String salpha="";
		int i=0;
		for (String label : pta.alphabet) {
			i++;
			salpha += "\"" + label + "\"";
			if(i!=pta.alphabet.size()){salpha +=",";}
		}
		s+= ",\n" + "\"alphabet\":[" +salpha + "],\n";
		s+= "\"cardclocks\":" + this.trans.get(0).get(0).tr.getParent().getNumClocks() + "\n";
		return (s+"}");
		//return(s+";\n"+y+";\n"+this.toStringCommentSAGE());//to add the commented graph in SAGE
	}

}
