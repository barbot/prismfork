//==============================================================================
//	
//	Copyright (c) 2002-
//	Authors:
//	* Dave Parker <david.parker@comlab.ox.ac.uk> (University of Oxford)
//	
//------------------------------------------------------------------------------
//	
//	This file is part of PRISM.
//	
//	PRISM is free software; you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation; either version 2 of the License, or
//	(at your option) any later version.
//	
//	PRISM is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with PRISM; if not, write to the Free Software Foundation,
//	Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//	
//==============================================================================

package pta;
import java.util.*;
public class DBMandBound extends DBM
{
	public LinkedList<Integer> lowerbounds; 
	public LinkedList<Integer> upperbounds;
	/**
	 * Construct an empty DBMandBound 
	 */
	public DBMandBound(PTA pta)
	{
		super(pta);
		this.lowerbounds= new LinkedList<Integer>(); 
		this.upperbounds= new LinkedList<Integer>();
	}
	public DBMandBound (DBM M){
		super(M.pta);
		LinkedList<Integer> upperbounds = new LinkedList<Integer>();
		LinkedList<Integer> lowerbounds = new LinkedList<Integer>();
		M.reduce();
		PTA pta = M.getPTA();
		int n = pta.numClocks + 1;
		//lower bounds
		for(int i = 1; i < n; i++){	
			if (M.d[0][i] != DB.INFTY){
				lowerbounds.add(i);
			}
		}
		//upper bounds
		for(int i = 1; i < n; i++){	
			if (M.d[i][0] != DB.INFTY){
				upperbounds.add(i);
			}
		}
		this.pta =M.pta;
		this.d =M.d;
		this.upperbounds=upperbounds;
		this.lowerbounds=lowerbounds;
	}



	//Basic test not used 	
	//	public boolean isUpperSplitted(){
	//		return (upperbounds.size()==1);
	//	}
	//
	//	public boolean isLowerSplitted(){
	//		return (lowerbounds.size()==1);
	//	}
	//	public boolean isSplitted(){
	//		return (this.isUpperSplitted()&this.isLowerSplitted());
	//	}
	//assuming that there is only one lower bound return it

	//	public int getLB() {
	//		return DB.getAbsoluteDiff(d[0][lowerbounds.get(0)]);
	//	}

	//assuming that there is only one upper bound return it
	//	public int getUB() {
	//		return DB.getAbsoluteDiff(d[upperbounds.get(0)][0]);
	//	}

	//	public boolean isPunctual()
	//	{
	//	}




	public DBMandBound CopyWithoutUB()
	{
		int i, j, n;
		DBMandBound copy = new DBMandBound(pta);
		n = this.pta.numClocks;
		copy.d = new int[n + 1][n + 1];
		for (i = 0; i < n + 1; i++) {
			for (j = 0; j < n + 1; j++) {
				copy.d[i][j] = d[i][j];
			}
		}
		copy.lowerbounds=this.lowerbounds;
		return copy;
	}

	public DBMandBound CopyWithoutLB()
	{
		int i, j, n;
		DBMandBound copy = new DBMandBound(pta);
		n = this.pta.numClocks;
		copy.d = new int[n + 1][n + 1];
		for (i = 0; i < n + 1; i++) {
			for (j = 0; j < n + 1; j++) {
				copy.d[i][j] = d[i][j];
			}
		}
		copy.upperbounds=this.upperbounds;
		return copy;
	}


	public LinkedList<DBMandBound> lowerSplit(){
		LinkedList<DBMandBound> res = new LinkedList<DBMandBound>();
		int k = lowerbounds.size();
		for (int i = 0; i < k; i++){
			int g = lowerbounds.get(i);
			DBMandBound res_aux;
			res_aux=this.CopyWithoutLB();
			res_aux.lowerbounds.add(g);
			for(int j = 0; j < k; j++ ){
				if (j != i){
					//res_aux.setConstraint(0, lowerbounds.get(j), 0);//useful?
					res_aux.addConstraint(g,lowerbounds.get(j), DB.add(this.d[0][lowerbounds.get(j)], DB.opposite((this.d[0][g]))));
				}
			}
			if (!res_aux.isEmpty()) {res.add(res_aux);}
		}
		return res;
	}


	public LinkedList<DBMandBound> upperSplit(){
		LinkedList<DBMandBound> res = new LinkedList<DBMandBound>();
		int k = upperbounds.size();
		for (int i = 0; i < k; i++){
			int g = upperbounds.get(i);
			DBMandBound res_aux = this.CopyWithoutUB();
			res_aux.upperbounds.add(g);
			for(int j = 0; j < k; j++ ){
				if (j != i){
					//res_aux.setConstraint(upperbounds.get(j), 0, DB.INFTY);//useful?
					res_aux.addConstraint(upperbounds.get(j), g , DB.add((this.d[upperbounds.get(j)][0]), DB.opposite(this.d[g][0])));
				}
			}
			if (!res_aux.isEmpty()) {res.add(res_aux);}
		}
		return res;
	}

	public DBM withoutBound(){
		return new DBM(this.pta,this.d);
	}

	/**
	 * Split guard
	 * @return a set of guards
	 */
	public LinkedList<DBM> mySplit (){
		LinkedList<DBMandBound> res0=new LinkedList<DBMandBound>(); 
		LinkedList<DBMandBound> res1=this.lowerSplit();
		LinkedList<DBM> res=new LinkedList<DBM>();
		for (DBMandBound dbm : res1) {
			res0.addAll(dbm.upperSplit());
		}
		//Remove punctual entries and canonicalise otherwise so that equality can be checked 
		for(int i = 0; i < res0.size(); i++){
			if (res0.get(i).isPunctual()) {res0.remove(i); i = i -1;}
			else {res0.get(i).canonicalise();res.add(res0.get(i).withoutBound());}//TODO Partial canonicalisations for efficiency
		}
		//remove the double results  
		for(int i = 0; i < res.size(); i++){
			for(int j = i +1; j < res.size(); j++){
				if (res.get(j).equals(res.get(i))) {res.remove(j); j = j -1;}
			}
		}
		return(res);
	}
}
